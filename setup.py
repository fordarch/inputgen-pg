from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='inputgen-pg',
    version='0.1.0',
    description='Just some input data generator.',
    long_description=readme,
    author='Seymore Butts',
    author_email='dont@me',
    url='',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)