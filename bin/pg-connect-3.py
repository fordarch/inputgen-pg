#!/usr/bin/python3.8

import psycopg2
import sys
import pprint
import random, string


def RandyWord():
   letters = string.ascii_lowercase
   return ''.join(random.choice(letters) for i in range(12))


def RandyNumber():
    return random.randint(0, 100)


def Main():
    conn_string = "host='192.168.1.117' dbname='testdb' user='tester' password='tester'"

    print(f"Connecting to database\n	->{conn_string}")
    
    with psycopg2.connect(conn_string) as conn:
        with conn.cursor() as curs:
            curs.execute("truncate table dataset1")

    with psycopg2.connect(conn_string) as conn:
        with conn.cursor() as curs:
            for i in range(0, 20):
                curs.execute("INSERT INTO dataset1 (dbkey, dbvalue) VALUES (%s, %s)", (RandyWord(), str(RandyNumber())))

    with psycopg2.connect(conn_string) as conn:
        with conn.cursor() as curs:
            curs.execute("SELECT * FROM dataset1")
            records = curs.fetchall()
            pprint.pprint(records)


if __name__ == '__main__':
    Main()
