#!/usr/bin/python3.8

import psycopg2
import sys
import pprint



def Main():
    conn_string = "host='192.168.1.117' dbname='testdb' user='tester' password='tester'"
    print(f"Connecting to database\n	->{conn_string}")
    conn = psycopg2.connect(conn_string)
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM dataset1")
    records = cursor.fetchall()
    pprint.pprint(records)


if __name__ == '__main__':
    Main()
