# README.md
Setting up portainer for data shenanigans

## Basics
Connect to desired server through MobaXterm
- fordarch prime - main server
- fordarch alpha - Bryan pc

Open portainer in browser
- 116:9000 - prime
- 117:9000 - alpha
	
Get new containers from dockerhub
- Start container
- Set up port

Data-grabbing scripts are in gitlab>fordarch>inputgen-pg>pg-connect-2.py
- Run scripts through dbeaver
- Write more scripts in vscode


